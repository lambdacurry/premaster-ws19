class LinkedList<T> {

  LinkedElem head, tail;

  class LinkedElem {
    
    public T value;
    public LinkedElem next;

    LinkedElem(T value) {
      this.value = value;
    }

    public void addNext(LinkedElem next) {
      this.next = next;
    }
  }

  LinkedList() {
    head = null;
    tail = null;
  }

  public void add(T value) {
    if (head==null) {
      head = new LinkedElem(value);
      tail = head;
    } else {
      tail.addNext(new LinkedElem(value));
      tail = tail.next;
    }
  }

  public T remove() {
    if (head==null) {
      return null;
    } else {
      T value = head.value;
      head = head.next;
      return value;
    }
  }

  public static void main(String[] args) {
    LinkedList<Integer> l = new LinkedList<Integer>();
    l.add(42);
    l.add(73);
    System.out.println(l.remove());
    l.add(7);
    System.out.println(l.remove());
    System.out.println(l.remove());
    System.out.println(l.remove());
  }
}
