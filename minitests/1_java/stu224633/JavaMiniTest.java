import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class FirstMiniTest {
    public void welcome() {
        System.out.println("Hello Kiel University Java Mini Test");
        System.out.println("Please Select one of the options" +
                "1.Sort array Elements (quick Sort)" +
                "2.Array to Linked List" +
                "3.Linked List to Array" +
                "4.Sortedly Insert a value in Linked List" +
                "5.Delete a value from Linked list (Delete anyone if multiple occurances)" +
                "6.Exit");
    }

    public void main(String[] args) {
        welcome();
        Scanner input = new Scanner(System.in);
        int preference = input.nextInt();
        System.out.println("Enter your preference:");
        if (preference == 1) {
            QuicksortTest qst = new QuicksortTest();
            qst.quiksortmain();
        } else if (preference == 2) {
            LinkedList<Integer> l = new LinkedList<Integer>();
            //Why Integer? 
            

            l.add("hello");
            l.add("Kiel");
            l.add("University");
            l.add("Java");
            l.add("Mini");
            l.add("Test");
            System.out.println(l.remove());
            System.out.println(l.remove());
            System.out.println(l.remove());
            System.out.println(l.remove());
            System.out.println(l.remove());
            System.out.println(l.remove());


        } else if (preference == 3) {

        } else if (preference == 4) {

        } else if (preference == 5) {

        } else if (preference == 6) {
            break;
        } else {
            System.out.println("invalid entry");
        }


    }
}

class QuicksortTest {
    public static void QuicksortTest(String[] A, int low, int high) {
        QuicksortTest(A, 0, A.length - 1);
        System.out.println(Arrays.toString(A));
        System.out.println(Arrays.toString(A));
    }

    public static void QuicksortTest(int[] A, int low, int high) {
        if (low < high + 1) {
            int p = partition(A, low, high);
            QuicksortTest(A, low, p - 1);
            QuicksortTest(A, p + 1, high);
        }
    }

    public static void swap(String[] A, int index1, int index2) {
        String temp = A[index1];
        A[index1] = A[index2];
        A[index2] = temp;
    }

    public static int getPivot(int low, int high) {
        Random rand = new Random();
        return rand.nextInt((high - low) + 1) + low;
    }

    public static int partition(String[] A, int low, int high) {
        swap(A, low, getPivot(low, high));
        int border = low + 1;
        for (int i = border; i <= high; i++) {
            if (A[i].compareTo(A[low])) {
                swap(A, i, border++);
            }
        }
        swap(A, low, border - 1);
        return border - 1;
    }


    public void quiksortmain() {
        String[] A = {"Hello", "Java", "Kiel", "Mini", "Test", "University"};
        QuicksortTest(A, 0, A.length - 1);
    }
}

class arrayToLinkedList {

}

class LinkedListMiniTest<T> {

    LinkedElemTest head, tail;
    public int counter = 0;

    class LinkedElemTest {

        public T value;
        public LinkedElemTest next;

        LinkedElemTest(T value) {
            this.value = value;
        }

        public void addNext(LinkedElemTest next) {
            this.next = next;
        }
    }

    LinkedListMiniTest() {
        head = null;
        tail = null;
    }

    public void add(T value) {
        if (head == null) {
            head = new LinkedElemTest(value);
            tail = head;
        } else {
            tail.addNext(new LinkedElemTest(value));
            tail = tail.next;
        }
        counter++;

    }


}
