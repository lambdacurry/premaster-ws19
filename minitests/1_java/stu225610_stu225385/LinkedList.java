
public class LinkedList {
		
	public Item head;
	public Item tail;

	class Item {
			
		public String value;
		public Item next;
			
		public Item(String value, Item next) {
			this.value = value;
			this.next = next;
		}
	}
		
	public LinkedList() {
		this.head = null;
		this.tail = null;
	}
		
	public void add(String value) {
		if(head == null) {
			head = new Item(value, null);
			tail = head;
		} else {
			tail.next = new Item(value, null);
			tail = tail.next;
		}
	}
	
	public String toString() {
		String items = "" + head.value;
		Item current = head.next;
		while(current != null) {
			items += "->" + current.value;
			current = current.next;
		}
		return items;			
	}
}	