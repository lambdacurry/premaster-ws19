import java.util.ArrayList;

public class QuickSort {
	public static int partition(String[] stringArray, int idx1, int idx2) {
		String pivotValue = stringArray[idx1];
		while (idx1 < idx2) {
			String value1;
			String value2;
			while ((value1 = stringArray[idx1]).compareTo(pivotValue) < 0) {
				idx1 = idx1 + 1;
			}
			while ((value2 = stringArray[idx2]).compareTo(pivotValue) > 0) {
				idx2 = idx2 - 1;
			}
			stringArray[idx1] = value2;
			stringArray[idx2] = value1;
		}
		return idx1;
	}

	public static void QuicksortString(String[] stringArray, int idx1, int idx2) {
		if (idx1 >= idx2) {
			return;
		}
		int pivotIndex = partition(stringArray, idx1, idx2);
		QuicksortString(stringArray, idx1, pivotIndex);
		QuicksortString(stringArray, pivotIndex + 1, idx2);
	}

	public static void QuicksortString(String[] stringArray) {
		QuicksortString(stringArray, 0, stringArray.length - 1);
	}

	public static ArrayList printArray(String[] stringArray) {
		ArrayList arrayList = new ArrayList();

		for (String s : stringArray) {
			System.out.print(s + " ");
			arrayList.add(s + " ");
		}

		return arrayList;
	}
}