import java.util.*;
class FirstMiniTest{

class LinkedList<T> { //good that you used a template!

  LinkedElem head, tail;

  class LinkedElem {

    public T value;
    public LinkedElem next;

    LinkedElem(T value) {
      this.value = value;
    }

    public void addNext(LinkedElem next) {
      this.next = next;
    }
  }

  LinkedList() {
    head = null;
    tail = null;
  }

  public void add(T value) { // should be sorted_insert!
    if (head==null) {
      head = new LinkedElem(value);
      tail = head;
    } else {
      tail.addNext(new LinkedElem(value));
      tail = tail.next;
    }
  }

  public T remove() { //you are able to remove only head, but should be any element
    if (head==null) {
      return -1;
    } else {
      T value = head.value;
      head = head.next;
      return value;
    }
  }

 public int sizee(){
    int len=0;

    LinkedElem el = head;
    //System.out.println(ch.next.value);
    if(head==null){
      return len;
    }
    else
    {
      do{
        len++;
        el = el.next;
      }while(el!=null);
      return len;
    }
  }

  public void toArray(){
//it will be better if you return an array for further work with it
    int len = sizee();
    int i = 0;
    //int [] x = new int[len];
    @SuppressWarnings("unchecked")
     Object[] x = (T[])new Object[len]; // thumb up
    if(head == null){
      x = null;
      System.out.println(Arrays.toString(x));
    }
    else{
       LinkedElem el = head;

       do{
        x[i] = el.value;
        el = el.next;
        i++;
      }while(el!=null);
      System.out.println(Arrays.toString(x)); // ok
    }
//you forgot a bracket here - code is not compiled!
//highly recommend you to install bracket matcher or smth. like this
    public static void main(String[] args){
        //System.out.println(Arrays.toString(args));
        int len = args.length;
        int c;
        Scanner sc = new Scanner(System.in);
        LinkedList<Integer> l = new LinkedList<Integer>();

        while(1)
        {
            System.out.println("1.Sort array Elements(Insertion sort/ quick sort");
            System.out.println("2.Array to linked list");
            System.out.println("3.Linked list to Array");
            System.out.println("4.Sortedly Insert a value in linked list");
            System.out.println("5.Delete a value from linked list");
            System.out.println("6.Exit");
            System.out.println("Enter an option : ");
            c = sc.nextInt();

            switch(c);{ // don't need a semicolon here
                case 1:
                    System.out.println("not completed");
                    break;
                case 2:
                    for(int i=0; i<len; i++){
// should be six cases
                    }
            }

        }
    }
}
//cannot find methods for: sorting an array, array to ll, sorted_insert
//Result: 0 + 0 + 15 + 0 + 0 + 0 = 15 -- welcome to the labs :)
