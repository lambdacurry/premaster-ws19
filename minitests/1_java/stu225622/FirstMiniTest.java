import java.util.Arrays;
import java.util.Scanner;

public class FirstMiniTest {

    String[] words;

    public static void swapper(String arr[],int small,int large){
        String temp = arr[small];
        arr[small]=arr[large];
        arr[large]=  temp;
    }

    public static void Sorter(int StartPos,int EndPos,String arr[]){

        //int Pivot = arr[EndPos];
        String Pivot = arr[StartPos+((EndPos-StartPos)/2)];
        int LessPointer = StartPos;
        int GreaterPointer = EndPos;

        while(LessPointer<=GreaterPointer){

            while(arr[LessPointer].compareTo(Pivot) < 0)
                LessPointer++;

            while(arr[GreaterPointer].compareTo(Pivot) > 0)
                GreaterPointer--;

            if(LessPointer<=GreaterPointer) {
                swapper(arr, LessPointer, GreaterPointer);
                LessPointer++;
                GreaterPointer--;
            }


        }

        if(StartPos<GreaterPointer)
            Sorter(StartPos, GreaterPointer, arr);

        if(LessPointer<EndPos)
            Sorter(LessPointer, EndPos, arr);




    }

    FirstMiniTest(String s){
        words = s.split("\\s+");

        for(int x = 0;x<words.length;x++){
            System.out.println(words[x]);
        }

    }



    public static void main(String[] args) {
        FirstMiniTest fmt = new FirstMiniTest("Hello Kiel University Java Mini Test");
        Scanner s = new Scanner(System.in);
        boolean check = true;
        while (check) {
            System.out.println("Please select one of the following options: " +
                    "1. Sort array Elements (Insertion Sort/ quick Sort)\n" +
                    "2. Array to Linked List\n" +
                    "3. Linked List to Array\n" +
                    "4. Sortedly Insert a value in Linked List\n" +
                    "5. Delete a value from Linked list (Delete anyone if multiple occurances)\n" +
                    "6. Exit");


            int choice = Integer.parseInt(s.nextLine());
            if (choice == 1) {
                Sorter(0, fmt.words.length - 1, fmt.words);
                System.out.println(Arrays.toString(fmt.words));
                // +18
            }
            if(choice == 6)
                check=false;

        }

        //program design structure +10
    }
    }

