
import java.util.Arrays;
import java.lang.String;

public class Main{


        LinkedElem head, tail;

        class LinkedElem {

            public String value;
            public LinkedElem next;

            LinkedElem(String value) {
                this.value = value;
            }

            public void addNext(LinkedElem next) {
                this.next = next;
            }
        }

        Main() {
            head = null;
            tail = null;
        }

        public void add(String value) {
            if (head==null) {
                head = new LinkedElem(value);
                tail = head;
            } else {
                tail.addNext(new LinkedElem(value));
                tail = tail.next;
            }
        }

        public String remove() {
            if (head==null) {
                return null;
            } else {
               String value = head.value;
                head = head.next;
                return value;
            }
        }

        public void unRemove(String value) {
            if (head==null) {
                head = new LinkedElem(value);
                tail = head;
            } else {
                LinkedElem temp = head;
                head =  new LinkedElem(value);
                head.addNext(temp);
            }
        }


        public int sizee(){
            int len=0;

            LinkedElem el = head;
            //System.out.println(ch.next.value);
            if(head==null){
                return len;
            }
            else
            {
                do{
                    len++;
                    el = el.next;
                }while(el!=null);
                return len;
            }
        }

        public void toArray(){
           int len = sizee();
            int i = 0;
            int [] x = new int[len];
            if(head == null){
                x = null;
                System.out.println(x);
            }
            else{
                LinkedElem el = head;

            }

        }







    public static void main(String[] args){
      //Insertion Sort
        String [] A={"Hello","java","Kiel","University","Mini", "Test"};
        System.out.println("Before Insertion Sorting"+ Arrays.toString(A));
        for(int index=1; index<=A.length-1;index++){
            String value= A[index];
            int comparingIndex= index-1;
            while(comparingIndex>=0 && A[comparingIndex].compareTo(value)>0){
                A[comparingIndex+1]=A[comparingIndex];
                comparingIndex--;

            }
            A[comparingIndex+1]= value;

        }

       System.out.println("After Sorting"+ Arrays.toString(A)); // Insertion sort ends +18

        Main l = new Main();
        l.add("Hello"); //add function is correct , but you have to add all the values of array. +9
        l.add("Java");
        System.out.println(l.remove());
        l.add("Kiel");
        System.out.println(l.remove());
        System.out.println(l.remove());
        System.out.println(l.remove()); //Remove needs parameter

        l.add("University");
        l.add("MiniTest");
        l.unRemove("MiniTest");
        l.unRemove("University");
        l.toArray();
        }
}

