class LinkedList<T> { // nice that you used a template!
case (1. Sort array Elements (Insertion Sort/ quick Sort)):
//this is certainly defined not in this way :(

  LinkedElem head, tail;

  class LinkedElem {

    public T value;
    public LinkedElem next;

    LinkedElem(T value) {
      this.value = value;
    }

    public void addNext(LinkedElem next) {
      this.next = next;
    }
  }

  LinkedList() {
    head = null;
    tail = null;
  }

  public void add(T value) {
    if (head==null) {
      head = new LinkedElem(value);
      tail = head;
    } else {
      tail.addNext(new LinkedElem(value));
      tail = tail.next;
    }
  }

  public T remove() { // the same here, you are able to remove only head value
    if (head==null) {
      return null;
    } else {
      T value = head.value;
      head = head.next;
      return value;
    }
  }

  public static void main(String[] args) {
    LinkedList<Integer> l = new LinkedList<Integer>();
    l.add(42);
    l.add(73);
    System.out.println(l.remove());
    l.add(7);
    System.out.println(l.remove());
    System.out.println(l.remove());
    System.out.println(l.remove());
  }
}

case (3. Linked List to Array):

public int getLength (linked list l){
if (l.head == null) }
return 0;
int n=0;
LinkedElement temp=l.head;
while (temp!=1.tail.next){
n++;
temp =temp.next;
}
Return n;
}

public T [] toArray(LinkedList l){ //ok
Int n =getlength(l);
var array=(t[] new Object[n];
linkedELem new1=head;
for (int i=0; i<n; i++){
arr[i]=new1.value;
new1=new1.next;
}
Return arr;
}

case (Sortedly Insert a value in Linked List):

class LinkedList // why did you define this class for one more time?
{



    class node1
    {
        int data;
        Node next;
        Node(int d) {data = d; next = null; }
    }


    void sortedInsert(Node new_node)
    {
         Node current;


         if (head == null || head.data >= new_node.data)
         {
            new_node.next = head;
            head = new_node;
         }
         else {


            current = head;

            while (current.next != null &&
                   current.next.data < new_node.data)
                   // won't work for strings! since strings are not comparable with > == <
                  current = current.next;

            new_node.next = current.next;
            current.next = new_node;
         }
     }


    Node newNode(int data)
    {
       Node x = new Node(data);
       return x;
    }


     void printList()
     {
         Node temp = head;
         while (temp != null)
         {
            System.out.print(temp.data+" ");
            temp = temp.next;
         }
     }


     public static void main(String args[])
     {
         LinkedList llist = new LinkedList();
         Node new_node;
         new_node = llist.newNode(5);
         llist.sortedInsert(new_node);
         new_node = llist.newNode(10);
         llist.sortedInsert(new_node);
         new_node = llist.newNode(7);
         llist.sortedInsert(new_node);
         new_node = llist.newNode(3);
         llist.sortedInsert(new_node);
         new_node = llist.newNode(1);
         llist.sortedInsert(new_node);
         new_node = llist.newNode(9);
         llist.sortedInsert(new_node);
         System.out.println("Created Linked List");
         llist.printList();
     }
}
// no methods for quicksort/insertion sort, array to list
// Result: 0 + 0 + 18 + 15 + 0 + 0 = 33 
