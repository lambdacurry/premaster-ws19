import java.util.*;
class LinkedListExam<T>{
	LinkedElem head;
	LinkedElem tail;
	
	LinkedListExam(){
		head = null;
		tail = null;
	}
	public class LinkedElem{
		T value;
		LinkedElem next;
		
		LinkedElem(T value){
			this.value = value;
			next = null;
		}
		public void addNext(LinkedElem next){
			this.next = next;
		}
		
	}
	public void add(T value){
		if(head == null){
			head = new LinkedElem(value);
			tail = head;
		}else{
			tail.addNext(new LinkedElem(value));
			tail = tail.next;
		}
	}
    public void addSortedly(T value){
		if(head == null){
			head = new LinkedElem(value);
			tail = head;
		}else{
			tail.addNext(new LinkedElem(value));
			tail = tail.next;
		}
		
		/*ArrayList<T> array = new ArrayList<T>();
		int listCount = 1;
		LinkedElem mark = head;
		do{
			array.add(mark.value);
			mark = mark.next;
			listCount++;
		}
		while(mark.next != null);
		System.out.println(array);*/
	}
	
	public T remove(){
		if(head == null){
			return null;
		}else{
			T deletedValue = head.value;
			head = head.next;
			return deletedValue;
		}
	}
	public T unRemove(T value){
		if(head == null){
			return null;
		}else{
		  LinkedElem tempHead = new LinkedElem(value);
          tempHead.addNext(head);
          head = tempHead;
          return tempHead.value;		  
		}
	}
	public ArrayList toArray(){
		if(head == null){
			return null;
		}
		 ArrayList<T> array = new ArrayList<T>();
		int listCount = 0;
		LinkedElem mark = head;
		
		do{
			array.add(mark.value);
			mark = mark.next;
			listCount++;
		}
		while(mark.next != null);
		array.add(mark.value);
		System.out.println(listCount);
		System.out.println(array);
        return null;
		
	}
}