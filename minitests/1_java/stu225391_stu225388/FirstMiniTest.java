import java.util.*;

class MergeSort
{
	void sort(String[] arr, int size)
	{
		divide(arr,0,size-1);
	}

	void divide(String[] arr, int low, int high)
	{
		int mid = (low+high)/2;
		if(low<high)
		{
			divide(arr, low, mid);
			divide(arr, mid+1,high);
		}
		conquer(arr,low,mid,high);
	}

	void conquer(String[] arr, int low, int mid, int high)
	{
		int i=0,j=0,l=low,m=mid+1,h=high,n=high-low+1;
		String[] copy = new String[n];
		while(l<=mid&&m<=high) copy[i++]=(arr[l].compareTo(arr[m])<0)?arr[l++]:arr[m++];
		while(l<=mid) copy[i++]=arr[l++];
		while(m<=high) copy[i++]=arr[m++];
		for(i=low;i<=high;i++) arr[i]=copy[j++];
	}
} //interesting that you choose that algorithm, but fully ok :)

class LinkedList<type>
{
	int size=0;
	Node head;

	class Node
	{
		type value;
		Node next=null;
	}

	LinkedList()
	{
		head = new Node();
	}

	void insert(type elem)
  // should be inserted sortedly, not only as the tail
	{
		Node n = head;
		while(n.next!=null) n=n.next;
		n.next = new Node();
		n.next.value = elem;
		size++;
	}

	void remove()
  //the same here, you are able to delete only tail element
	{
		Node n = head;
		while(n.next!=null) n=n.next;
		n = null;
		size--;
	}

	void unremove(type elem)
	{
		Node n = new Node();
		n.value = elem;
		n.next = head.next;
		head.next = n;
		size++;
	}

	type valueAt(int i)
	{
		Node n = head;
		int ct = 0;
		while(ct<i)
		{
			n=n.next;
			ct++;
		}
		return n.value;
	}

	type[] toArray() //ok
	{
		Node n = head.next;
		type[] arr = (type[])new Object[size]; //nice
		int i=0;
		while(i<size)
		{
			arr[i++] = n.value;
			n = n.next;
		}
		return arr;
	}
}

class FirstMiniTest
{
	String[] input_arr;
	LinkedList input_list;
 //you could get those things as parameters to the methods, but ok
	FirstMiniTest(String[] input)
	{
		this.input_arr = input;
	}

	void sort()
	{
		MergeSort sorter = new MergeSort();
		sorter.sort(input_arr,input_arr.length);
		System.out.println("\t\n"+Arrays.toString(input_arr)+"\n");
	}

	void a2l() //ok
	{
		LinkedList <String> list = new LinkedList();
		for(int i=0;i<input_arr.length;i++)
		{
			list.insert(input_arr[i]);
		}
		input_list = list;
		for(int i=1;i<input_arr.length+1;i++)
		{
			System.out.print(list.valueAt(i));
			if(i==input_arr.length) continue;
			System.out.print("-->");
		}
		System.out.println();
	}

	public static void main(String args[])
	{
		int option;
		Scanner sc = new Scanner(System.in);
		FirstMiniTest ft = new FirstMiniTest(args);
		while(true)
		{
			System.out.println("\n1.Sort\n2.Array2LinkedList\n3.LinkedList2Array\n4.Insert into List Sortedly\n5.List Element Delete\n6.Quit\n");
			System.out.print("Choose an Option: ");
			option = Integer.parseInt(sc.nextLine());
			switch(option)
			{
				case 1: ft.sort(); break;
				case 2: ft.a2l(); break;
				//case 3: l2a(); break;
				//case 4: insert(); break;
				//case 5: delete(); break;
        //pity that they are commented :(
				case 6: return;
				default: System.out.println("Invalid input"); break;
			}
		}
	}
}
//Result: 18 + 18 + 18 + 0 + 0 + 5 = 59 --good!
