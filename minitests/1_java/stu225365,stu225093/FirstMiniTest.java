import java.util.*;

public class FirstMiniTest {

	Node head,tail;
	public FirstMiniTest() {
		this.head = null;
		this.tail = null;
	}

	public void addNode(String value) {
		if(head == null) {
			head = tail = new Node(value);
			System.out.println(head.previous + "-"+head+ "-"+head.next);
		}else {
			tail.addNext(new Node(value));
			tail = tail.next;
		}
	}

	public String remove() { //should remove from any position, not only from head
		if(head == null) {
			return null;
		}else {
			String value = head.value;
			head = head.next;
			return value;
		}

	}

	public static void swap(String [] array, int pos_1, int pos_2) {
		String temp = array[pos_1];
		array[pos_1] = array[pos_2];
		array[pos_2] = temp;
		}

	public static int partition(String inputArray[],int high, int low) {
		String pivot = inputArray[high];
		int i = low-1;
		for(int j = low; j<=high; j++) {
			if((inputArray[j].compareTo(pivot)) == -1) { //nice
				i++;
				swap(inputArray,i,j);
			}
		}
		swap(inputArray,i+1,high);
		return i+1;

	}

	public static void quickSort(String inputArray[],int high, int low) {//ok
		if (low < high) {
			int partition_index = partition(inputArray,high,low);
			quickSort(inputArray,partition_index - 1,  low);
			quickSort(inputArray,high,partition_index + 1);

		}
	}

	public static void main(String[] args) {
		String[] inputArray = {"Hello","Kiel","University","Java","Mini","Test"};
		FirstMiniTest l = new FirstMiniTest();
		for(int i = 0; i<inputArray.length; i++) {
			l.addNode(inputArray[i]);
		}
		quickSort(inputArray,inputArray.length-1,0);
		System.out.println(Arrays.toString(inputArray));
//actually you should implement here a case structure, so user can choose which method to call

	}

}
// no methods for array to linked list, linked list to array, insert and delete (delete is wrong)
// Result: 18 + 0 + 0 + 0 + 0 + 0 = 18 --welcome to the labs :)
