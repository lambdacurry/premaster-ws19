public class Node {
	public String value;
	public Node next;
	public Node previous;
	
	public Node(String value) {
		this.value = value;
	}
	
	public void addNext(Node next) {
		this.next = next;
	}
	
	public void addPrevious(Node previous) {
		previous.previous = this;
	}
}
