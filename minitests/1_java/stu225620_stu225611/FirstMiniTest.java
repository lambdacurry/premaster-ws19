import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class FirstMiniTest {

	public static void insertionSort(String[] array) { //ok!

		for (int i = 1; i < array.length; i++) {

			String key = array[i];
			int j = i - 1;

			while (j >= 0 && array[j].compareTo(key) > 0) {
				array[j + 1] = array[j];
                j = j - 1;
			}

			array[j + 1] = key;

		}

	}



	public static void main(String[] args) {


		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter an options: " );
		int o = scanner.nextInt();

		switch (o) {
			case 1:
				insertionSort(args);
//since your function is void and prints nothing, that's not working
				break;
			case 2:
				LinkedList ll = new LinkedList(Arrays.asList(args));
//you use there a standard java's LinkedList, but not an instance of your class LinkList
			    Iterator iterator = ll.iterator();
			    while (iterator.hasNext()) {

		            System.out.print((String) iterator.next());
		            System.out.print(" --> ");
			     }
				break;
			case 3:
				LinkList ls = new LinkList();
				ls.head = new LinkNode("java"); //should use user's input
				ls.head.next = new LinkNode("home");
				ls.head.next.next = new LinkNode("test");
				String[] array1 = ls.toArray();
				System.out.println(Arrays.toString(array1));
			case 4:
				break; // :(
			case 5:
				LinkList ls1 = new LinkList();
				ls1.head = new LinkNode("java");//should use user's input
				ls1.head.next = new LinkNode("home");
				ls1.head.next.next = new LinkNode("test");
				ls1.deleteLink("home");
				System.out.println(Arrays.toString(ls1.toArray()));
		default:
			break;
		}

	}



}
//Result: 18 + 0 + 18 + 0 + 18 + 3 = 57 --good!
