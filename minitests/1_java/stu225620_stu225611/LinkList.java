
public class LinkList {

	public LinkNode head;

	public String[] toArray() {//ok

		LinkNode temp = head;
		int len = 0, i = 0;
		while (temp != null) {
			temp = temp.next;
			len++;
		}

		String[] array = new String[len];

		temp = head;
		while (temp != null) {
			array[i] = temp.value;
			i++;
			temp = temp.next;
		}

		return array;

	}

	public void deleteLink(String x) { //ok

		LinkNode prev = null;
		LinkNode current = head;

		if (current == null) {
			return;
		}

		if (head.value.equals(x)) {
			head = current.next;
		}

		while (current != null) {

			if (current.value.equals(x)) {
				prev.next = current.next;
			}

			prev = current;
			current = current.next;
		}

	}

}
//no insert function
