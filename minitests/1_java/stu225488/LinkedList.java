

public class LinkedList {

	LinkedElem head , tail;
	int count;
	
	public LinkedList() {
		head = null;
		tail = null;
	}
	public class LinkedElem
	{
		
		String value;
		LinkedElem next;
		
		public LinkedElem(String value) {
			this.value = value;
		}
	
		
	}
	

	public int getSize() {
		LinkedElem tempHead = head;
		int cnt = 0;
		while(tempHead != null)
		{
			cnt ++;
			tempHead = tempHead.next;
		}
		return cnt;
	}
	
	public String[] toArray() {
		int size= getSize();
		int i= 0;
		LinkedElem tempHead = head;
		String[] a = new String[size];
		while(tempHead != null)
		{
			a[i] = tempHead.value;
			tempHead = tempHead.next;
			i++;
			
			
		}
		return a;
	}
	
	public void Add(String value) {
		
		if(head == null) {
			head = new LinkedElem(value);
			tail = head;
			
		}
		else
		{
			tail.next = new LinkedElem(value);
			tail = tail.next;
			
		}
	}
	
	public String Remove() 
	{
		if(head != null) {
			String tempHeadValue  = head.value;
			head = head.next;
			return tempHeadValue;
		}else {
			
			return null;
		}
		
	}
	
	
	
	public void UnRemove(String value) {
		
		if(head == null) {
			
			head = new LinkedElem(value);
			tail = head;
		}
		else
		{
			LinkedElem tempHead = new LinkedElem(value);
			tempHead.next = head;
			head = tempHead;
		}

	}
	
}

